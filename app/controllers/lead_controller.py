from flask import jsonify, request, current_app
from app.models.lead_model import Lead
from app.exceptions.lead_exceptions import (
    MissingKeysException,
    InvalidKeysException,
    ValuesAreNotInstanceOfTypeStringException,
    NotUniqueValuesException,
    InvalidPhoneFormatException
)


def create_lead():

    try:

        data: dict = request.get_json()

        Lead.check_keys(data)

        Lead.check_values(data)

        data = Lead.input_date_values(data)

        lead: Lead = Lead(**data)

        current_app.db.session.add(lead)
        current_app.db.session.commit()

        return jsonify(lead), 201

    except MissingKeysException as e:

        return jsonify({
            'error': e.get_message()
        }), 400

    except InvalidKeysException as e:

        return jsonify({
            'error': e.get_message()
        }), 400

    except ValuesAreNotInstanceOfTypeStringException as e:

        return jsonify({
            'error': e.get_message()
        }), 400

    except NotUniqueValuesException as e:

        return jsonify({
            'error': e.get_message()
        }), 400

    except InvalidPhoneFormatException as e:

        return jsonify({
            'error': e.get_message()
        }), 400


def get_all():

    data = Lead.query.all()

    if data != []:
        return jsonify(data), 200
    else:
        return jsonify({
            'error': 'No data found.'
        }), 404
    

def update_lead():

    try:

        req: dict = request.get_json()

        Lead.check_update_and_delete_keys(req)

        Lead.check_update_and_delete_values(req)

        lead: Lead = Lead.query.filter_by(email = req['email']).first()

        if lead is None:
            return jsonify({
                'error': 'No data found.'
            }), 404

        data: dict = Lead.update_values(lead.visits)

        Lead.query.filter_by(email = req['email']).update(data)
        current_app.db.session.commit()

        return '', 204

    except MissingKeysException as e:

        return jsonify({
            'error': e.get_message()
        }), 400

    except InvalidKeysException as e:

        return jsonify({
            'error': e.get_message()
        }), 400

    except ValuesAreNotInstanceOfTypeStringException as e:

        return jsonify({
            'error': e.get_message()
        }), 400


def delete_lead():

    try:

        req: dict = request.get_json()

        Lead.check_update_and_delete_keys(req)

        Lead.check_update_and_delete_values(req)

        lead: Lead = Lead.query.filter_by(email = req['email']).first()

        if lead is None:
            return jsonify({
                'error': 'No data found.'
            }), 404

        current_app.db.session.delete(lead)
        current_app.db.session.commit()

        return '', 204

    except MissingKeysException as e:

        return jsonify({
            'error': e.get_message()
        }), 400

    except InvalidKeysException as e:

        return jsonify({
            'error': e.get_message()
        }), 400

    except ValuesAreNotInstanceOfTypeStringException as e:

        return jsonify({
            'error': e.get_message()
        }), 400
