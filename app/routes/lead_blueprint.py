from flask import Blueprint
from app.controllers.lead_controller import (
    create_lead,
    delete_lead,
    get_all,
    update_lead,
    delete_lead
)


bp: Blueprint = Blueprint('bp_leads', __name__, url_prefix='/leads')

bp.post('')(create_lead)
bp.get('')(get_all)
bp.patch('')(update_lead)
bp.delete('')(delete_lead)
