from app.configs.database import db
from dataclasses import dataclass
from datetime import datetime
import re
from app.exceptions.lead_exceptions import (
    MissingKeysException,
    InvalidKeysException,
    ValuesAreNotInstanceOfTypeStringException,
    NotUniqueValuesException,
    InvalidPhoneFormatException
)


@dataclass
class Lead(db.Model):

    name: str
    email: str
    phone: str
    creation_date: datetime
    last_visit: datetime
    visits: int

    __tablename__ = 'leads'

    id = db.Column(db.Integer, primary_key = True)
    name = db.Column(db.String(50), nullable = False)
    email = db.Column(db.String(100), nullable = False)
    phone = db.Column(db.String(14), nullable = False)
    creation_date = db.Column(db.DateTime)
    last_visit = db.Column(db.DateTime)
    visits = db.Column(db.Integer, default = 1)


    @staticmethod
    def check_keys(data: dict) -> None:

        REQUIRED_KEYS: tuple = ('name', 'email', 'phone')
        missing_keys: list = []
        invalid_keys: list = []

        for key in REQUIRED_KEYS:
            if key not in data:
                missing_keys.append(key)

        if missing_keys != []:
            raise MissingKeysException(f'The keys {missing_keys} are missing.')

        for key in data:
            if key not in REQUIRED_KEYS:
                invalid_keys.append(key)

        if invalid_keys != []:
            raise InvalidKeysException(f'The keys {invalid_keys} are invalid.')


    @staticmethod
    def check_values(data: dict) -> None:

        not_unique_values: list = []
        not_instance_of_type_string: list = []

        for value in data.values():
            if not isinstance(value, str):
                not_instance_of_type_string.append(value)

        if not_instance_of_type_string != []:
            raise ValuesAreNotInstanceOfTypeStringException(f'The values {not_instance_of_type_string} are not instace of type string.')

        lead_by_email: Lead = Lead.query.filter_by(email = data['email']).first()
        lead_by_phone: Lead = Lead.query.filter_by(phone = data['phone']).first()

        if lead_by_email is not None:
            not_unique_values.append(data['email'])

        if lead_by_phone is not None:
            not_unique_values.append(data['phone'])

        if not_unique_values != []:
            raise NotUniqueValuesException(f'The values {not_unique_values} already exists.')

        phone_pattern: str = '[\(][0-9]{2}[\)][0-9]{5}[\-][0-9]{4}'

        phone_string: str = data['phone']

        check_phone_format = re.fullmatch(phone_pattern, phone_string)

        if check_phone_format is None:
            raise InvalidPhoneFormatException('The phone format is invalid, please input a phone with the following format: (xx)xxxxx-xxxx')


    @staticmethod
    def input_date_values(data: dict) -> dict:

        data['creation_date'] = datetime.now()
        data['last_visit'] = datetime.now()        

        return data


    @staticmethod
    def check_update_and_delete_keys(data: dict) -> None:

        REQUIRED_KEYS: tuple = ('email',)
        missing_keys: list = []
        invalid_keys: list = []

        for key in REQUIRED_KEYS:
            if key not in data:
                missing_keys.append(key)

        if missing_keys != []:
            raise MissingKeysException(f'The keys {missing_keys} are missing.')

        for key in data:
            if key not in REQUIRED_KEYS:
                invalid_keys.append(key)

        if invalid_keys != []:
            raise InvalidKeysException(f'The keys {invalid_keys} are invalid.')


    @staticmethod
    def check_update_and_delete_values(data: dict) -> None:

        if not isinstance(data['email'], str):
            raise ValuesAreNotInstanceOfTypeStringException('The email value is not instance of type string.')


    @staticmethod
    def update_values(visits: int) -> dict:

        data: dict = {}

        data['last_visit'] = datetime.now()
        data['visits'] = visits + 1

        return data
