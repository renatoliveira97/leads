class CustomizedException(Exception):
    
    def __init__(self, message):
        self.message = message


    def get_message(self):
        return self.message


class MissingKeysException(CustomizedException):

    def __init__(self, message):
        CustomizedException.__init__(self, message)


class InvalidKeysException(CustomizedException):

    def __init__(self, message):
        CustomizedException.__init__(self, message)


class ValuesAreNotInstanceOfTypeStringException(CustomizedException):

    def __init__(self, message):
        CustomizedException.__init__(self, message)


class NotUniqueValuesException(CustomizedException):

    def __init__(self, message):
        CustomizedException.__init__(self, message)


class InvalidPhoneFormatException(CustomizedException):

    def __init__(self, message):
        CustomizedException.__init__(self, message)
